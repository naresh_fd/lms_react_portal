/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody } from 'reactstrap';
import info from '../../assets/img/icons/info.svg';
import tick from '../../assets/img/icons/tick_type1.svg';

const PriceCard = ({ data }) => {
  return (
    <Card>
      <CardBody className="pt-5 pb-5 d-flex flex-lg-column flex-md-row flex-sm-row flex-column">
        <div className="price-top-part">
          <i className={`large-icon ${data.icon}`} />
          <h5 className="mb-0 font-weight-semibold -1 mb-4">{data.title}</h5>
          <h2 className="text-large price mb-0 pb-1 pt-1 text-default">
            {data.price}
          </h2>
          <p className="text-muted text-small detail">{data.detail}</p>
          <Link to="/usersignup">
            <Button className="btn subscribe"> {data.btnText} </Button>{' '}
          </Link>
          <hr className="mt-5 mb-5" />
        </div>
        <div className="pl-3 pr-3 pt-3 pb-0 d-flex price-feature-list flex-column flex-grow-1">
          <ul className="list-unstyled feature_list">
            <li>{data.ext}</li>
            {data.features.map((feature, index) => {
              return (
                <li key={index}>
                  <img src={tick} className="pr-2" alt="tick" />
                  {/* <i className="fas fa-check" /> */}
                  <p className="mb-0">{feature}</p>
                  <img src={info} className="pl-2" alt="info" />
                </li>
              );
            })}
          </ul>
        </div>
      </CardBody>
    </Card>
  );
};

export default React.memo(PriceCard);
