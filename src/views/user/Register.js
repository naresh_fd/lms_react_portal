import React, { useState } from 'react';
import {
  Breadcrumb,
  Button,
  Card,
  CardBody,
  CardTitle,
  // CardTitle,
  Col,
  Collapse,
  Container,
  Row,
  // Card,
  // CardImg,
  // CardText,
  // CardBody,
  // CardTitle,
  // CardSubtitle,
  // Button,
  // Row,
} from 'reactstrap';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/BalnkNav.scss';
import '../../assets/css/sass/views/register.scss';
import PriceCard from '../../components/cards/PriceCard';
import pricesData from '../../data/prices';
import { Colxx, Separator } from '../../components/common/CustomBootstrap';
import { getCurrentLanguage } from '../../helpers/Utils';

const Register = ({ match }) => {
  // const [collapse, setCollapse] = useState(false);
  const [selectedAccordion, setSelectedAccordion] = useState(1);
  const locale = getCurrentLanguage();

  return (
    <div>
      <div className="bg">
        <BlankNav />
        <Container>
          <Row className="equal-height-container mb-5">
            <Col className="md5">
              <h2 className="text-center mt-5">
                Secure and Powerful CMS for your Organization
              </h2>
            </Col>

            <Colxx xxs="12" />
            {/* <container className="pr-2 pl-2"> */}
            {pricesData[locale].map((item) => {
              return (
                <Colxx
                  md="12"
                  lg="4"
                  className="col-item pricecard mx-auto mb-4"
                  key={item.title}
                  id={item.id}
                >
                  <PriceCard data={item} />
                </Colxx>
              );
            })}
            {/* </container> */}
            <div className="col t_c">
              Local taxes (VAT, GST, etc.) will be charged in addition to the
              prices mentioned.
            </div>
          </Row>
        </Container>
      </div>

      <div className="text-center bg-white pb-5">
        <Container>
          <Row className="equal-height-container mb-5">
            <Col className="md5">
              <h2 className="text-center mt-5" style={{ width: '70%' }}>
                30-day free trial for CMS Pro plan. No credit card required.
              </h2>
              <Button className="btn subscribe">Try Now </Button>
            </Col>

            <hr className="mt-5 mb-5" />
          </Row>

          <Row>
            <Colxx xxs="12">
              <Breadcrumb heading="menu.collapse" match={match} />
              <Separator className="mb-5" />
            </Colxx>
          </Row>

          <Row>
            <Colxx xxs="12" className="mb-4">
              <Card style={{ boxShadow: 'none' }}>
                <CardBody>
                  <CardTitle>
                    <h2 style={{ color: '#1C1D21' }}>
                      Frequently asked questions
                    </h2>
                  </CardTitle>
                  <>
                    <div className="border">
                      <Button
                        color="link"
                        onClick={() => setSelectedAccordion(1)}
                        aria-expanded={selectedAccordion === 1}
                      >
                        How does user licensing work?
                      </Button>
                      <Collapse isOpen={selectedAccordion === 1}>
                        <div className="p-4">
                          There are many variations of passages of Lorem Ipsum
                          available, but the majority have suffered alteration
                          in some form, by injected humour, or randomised words
                          which dont look even slightly believable.
                        </div>
                      </Collapse>
                    </div>
                    <div className="border">
                      <Button
                        color="link"
                        onClick={() => setSelectedAccordion(2)}
                        aria-expanded={selectedAccordion === 2}
                      >
                        Can I mix and match two or more plans for the same
                        organization?
                      </Button>
                      <Collapse isOpen={selectedAccordion === 2}>
                        <div className="p-4">
                          2. Anim pariatur cliche reprehenderit, enim eiusmod
                          high life accusamus terry richardson ad squid. 3 wolf
                          moon officia aute, non cupidatat skateboard dolor
                          brunch. Food truck quinoa nesciunt laborum eiusmod.
                          Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                          it squid single-origin coffee nulla assumenda
                          shoreditch et. Nihil anim keffiyeh helvetica, craft
                          beer labore wes anderson cred nesciunt sapiente ea
                          proident. Ad vegan excepteur butcher vice lomo.
                          Leggings occaecat craft beer farm-to-table, raw denim
                          aesthetic synth nesciunt you probably havent heard of
                          them accusamus labore sustainable VHS.
                        </div>
                      </Collapse>
                    </div>
                    <div className="border">
                      <Button
                        color="link"
                        onClick={() => setSelectedAccordion(3)}
                        aria-expanded={selectedAccordion === 3}
                      >
                        What types of payment do you accept?
                      </Button>
                      <Collapse isOpen={selectedAccordion === 3}>
                        <div className="p-4">
                          3. Anim pariatur cliche reprehenderit, enim eiusmod
                          high life accusamus terry richardson ad squid. 3 wolf
                          moon officia aute, non cupidatat skateboard dolor
                          brunch. Food truck quinoa nesciunt laborum eiusmod.
                          Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                          it squid single-origin coffee nulla assumenda
                          shoreditch et. Nihil anim keffiyeh helvetica, craft
                          beer labore wes anderson cred nesciunt sapiente ea
                          proident. Ad vegan excepteur butcher vice lomo.
                          Leggings occaecat craft beer farm-to-table, raw denim
                          aesthetic synth nesciunt you probably havent heard of
                          them accusamus labore sustainable VHS.
                        </div>
                      </Collapse>
                    </div>
                    <div className="border">
                      <Button
                        color="link"
                        onClick={() => setSelectedAccordion(3)}
                        aria-expanded={selectedAccordion === 3}
                      >
                        Is my data safe?
                      </Button>
                      <Collapse isOpen={selectedAccordion === 3}>
                        <div className="p-4">
                          3. Anim pariatur cliche reprehenderit, enim eiusmod
                          high life accusamus terry richardson ad squid. 3 wolf
                          moon officia aute, non cupidatat skateboard dolor
                          brunch. Food truck quinoa nesciunt laborum eiusmod.
                          Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                          it squid single-origin coffee nulla assumenda
                          shoreditch et. Nihil anim keffiyeh helvetica, craft
                          beer labore wes anderson cred nesciunt sapiente ea
                          proident. Ad vegan excepteur butcher vice lomo.
                          Leggings occaecat craft beer farm-to-table, raw denim
                          aesthetic synth nesciunt you probably havent heard of
                          them accusamus labore sustainable VHS.
                        </div>
                      </Collapse>
                    </div>
                  </>
                </CardBody>
              </Card>
            </Colxx>
          </Row>
        </Container>
      </div>

      <div className="footer p-3 text-center text-muted">
        © 2020, LMS Corporation Ltd. All Rights Reserved.{' '}
      </div>
    </div>
  );
};

export default Register;
