/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ButtonGroup, Col, Row } from 'reactstrap';
import { Button, TextField } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useForm, useFieldArray, Controller } from 'react-hook-form';
import DeleteIcon from '@material-ui/icons/Delete';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';

export default function AddAssociates() {
  const [associateName, setAssociatesName] = useState();
  const [email, setemail] = useState();

  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'associates',
  });
  // const inputApp = register();

  const history = useHistory();

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard mx-auto"
        style={{
          width: '35%',
        }}
      >
        <h2 className="text-center">Try CMS Premium free for 30day</h2>
        <span className="Topbox">Step 2 of 3</span>
        <div className="card pt-5 pb-5 pl-5 pr-5">
          <h6 className="font-weight-bold text-center pb-4">
            Invite your associates right away!{' '}
          </h6>
          <p className="text-muted col-10 mx-auto">
            Associates are the ones who would manage your leads. Add your
            associates and get them notified through mail. You can add them
            later as well.
          </p>
          <form
            onSubmit={handleSubmit((data) => {
              sessionStorage.setItem('associates', JSON.stringify(data));
              console.log(data);
              history.push('/billingdetails');
            })}
          >
            <Row>
              {' '}
              {fields.map(({ id }, index) => {
                return (
                  <Row key={id} className="associates">
                    <Col className="mb-3 col-5 pr-0">
                      <TextField
                        // ref={inputApp}
                        fullWidth
                        name={`items[${index}]`}
                        label="Associate's name"
                        // name="associates"
                        variant="outlined"
                        onChange={(e) => setAssociatesName(e.target.value)}
                        value={associateName}
                        {...register('associates', {
                          required: 'Associate name is required',
                          minLength: {
                            value: 5,
                            message:
                              'Minimum 5 Characters Required for User Name',
                          },
                        })}
                      />
                      {errors.associates && (
                        <p className="error"> {errors.associates.message}</p>
                      )}
                    </Col>
                    <Col className="mb-3 col-5 pr-1 " id={`associates${index}`}>
                      <TextField
                        type="email"
                        fullWidth
                        name={`items[${index}].email`}
                        label="Associate's email"
                        variant="outlined"
                        value={email}
                        onChange={(e) => setemail(e.target.value)}
                        id="email"
                        aria-invalid={errors.email ? 'true' : 'false'}
                        {...register('email', {
                          required: "Associate's email is required",
                          pattern: {
                            value: /\S+@\S+\.\S+/,
                            message:
                              'Entered value does not match email format',
                          },
                        })}
                      />
                      {errors.email && (
                        <p className="error">{errors.email.message}</p>
                      )}
                    </Col>
                    <Col className="mb-3 ">
                      {' '}
                      <DeleteIcon
                        type="button"
                        onClick={() => remove(index)}
                      />{' '}
                    </Col>
                  </Row>
                );
              })}
            </Row>
            <Button
              className="addAssociate"
              type="button"
              onClick={() => append({})}
            >
              Append
            </Button>
            <div className="button_sec_a" variant="contained" color="primary">
              <Button color="default" onClick={history.goBack}>
                <ArrowBackIcon />
              </Button>

              <Button
                className="btn_next col-12"
                color="primary"
                variant="contained"
                type="submit"
              >
                Next <i className="fas fa-arrow-right ml-3" />
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
// export default AccountSetup1;
