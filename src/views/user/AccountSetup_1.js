import React from 'react';
import { useHistory } from 'react-router-dom';
import { ButtonGroup, Row } from 'reactstrap';
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';
// import HistoryBack from '../app/HistoryBack';

const AccountSetup1 = () => {
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    const checkedValues = Array.from(e.target.course).map((el) => [
      el.id,
      el.checked,
    ]);
    console.log(Object.fromEntries(checkedValues));
    history.push('/addassociates');
  };

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard mx-auto"
        style={{
          width: '25%',
        }}
      >
        <h2 className="text-center">Try CMS Premium free for 30day</h2>
        <span className="Topbox">Step 1 of 3</span>
        <div className="card pt-5 pb-5 pl-5 pr-5">
          <h6 className="font-weight-bold text-center pb-4">
            First, tell us a bit about yourself and your company
          </h6>

          <form onSubmit={handleSubmit}>
            <Row className="col-md-12 pl-5 m4 mx-auto accstep1">
              {' '}
              {[
                'Sales',
                'Marketing',
                'Finance',
                'R&D',
                'Managers',
                'Others',
              ].map((i) => (
                <label key={i} id={i} htmlFor={i} className="mr-4 mb-5">
                  <input
                    type="checkbox"
                    id={i}
                    name="course"
                    className="mr-2"
                  />
                  {i}
                </label>
              ))}{' '}
            </Row>
            <Row>
              <ButtonGroup
                className="button_sec"
                variant="contained"
                color="primary"
              >
                <Button color="link" onClick={history.goBack}>
                  <ArrowBackIcon />
                </Button>

                <Button
                  className="btn_next col-12"
                  color="primary"
                  variant="contained"
                  type="submit"
                >
                  Next <i className="fas fa-arrow-right ml-3" />
                </Button>
              </ButtonGroup>
            </Row>
          </form>
        </div>
      </div>
    </div>
  );
};
export default AccountSetup1;
