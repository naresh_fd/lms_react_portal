import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { useForm } from 'react-hook-form';
import { Button, TextField } from '@material-ui/core';
import { ArrowForwardSharp } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';
import premumLogo from '../../assets/img/premumLogo.svg';

const BillingAddress = () => {
  const [firstName, setfirstName] = useState();
  const [lastname, setlastName] = useState();
  const [address, setAddress] = useState();
  const [zipCode, setZipCode] = useState();
  const [city, setCity] = useState();
  const [country, setCountry] = useState();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
  });

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard  mx-auto"
        style={{
          width: '40%',
        }}
      >
        <Row className="card p0   ">
          <div className="card-header">
            <Row>
              <Col className="col-md-3 ">
                <img
                  src={premumLogo}
                  className="float-left pl-3"
                  alt="premium Logo"
                />
              </Col>
              <Col className="col-md-8 pl-2 text-left">
                <h2 className="align-middle m0 pt-4">CMS Pro plan</h2>
                <p className="text-muted m-0">
                  Nunc scelerisque tincidunt elit. Vestibulum non mi ipsum. Cras
                  pretium suscipit tellus sit amet aliquet. Vestibulum maximus
                  lacinia massa non porttitor.
                </p>
                <Button variant="default">Change Plan</Button>
              </Col>
              <Col className="price">0</Col>
            </Row>
          </div>
          <form
            className="p-5"
            onSubmit={handleSubmit((data) => {
              sessionStorage.setItem('billingDetials', JSON.stringify(data));
              console.log(data);
              history.push('/cardgdetails');
            })}
          >
            <Row className="mb-4   bill">
              <Col className="mx-auto">
                <TextField
                  className="col-md-12"
                  id="UserName"
                  label="UserName"
                  name="UserName"
                  variant="outlined"
                  onChange={(e) => setfirstName(e.target.value)}
                  value={firstName}
                  {...register('UserName', {
                    required: 'Username is required',
                    minLength: {
                      value: 5,
                      message: 'Minimum 5 Characters Required for User Name',
                    },
                  })}
                />
                {errors.UserName && (
                  <p className="error"> {errors.UserName.message}</p>
                )}
              </Col>
              <Col>
                <TextField
                  className="col-md-12"
                  id="lastname"
                  label="lastname"
                  name="lastname"
                  variant="outlined"
                  onChange={(e) => setlastName(e.target.value)}
                  value={lastname}
                  {...register('lastname', {
                    required: 'lastname is required',
                    minLength: {
                      value: 5,
                      message: 'Minimum 5 Characters Required for last Name',
                    },
                  })}
                />
                {errors.lastname && (
                  <p className="error"> {errors.lastname.message}</p>
                )}{' '}
              </Col>
            </Row>
            <Row className="mb-4   bill">
              <Col className="mx-auto col-8">
                <TextField
                  className="col-md-12"
                  id="address"
                  label="address"
                  name="address"
                  variant="outlined"
                  onChange={(e) => setAddress(e.target.value)}
                  value={address}
                  {...register('address', {
                    required: 'address is required',
                    minLength: {
                      value: 5,
                      message: 'Address should be at least 5 characters',
                    },
                  })}
                />
                {errors.address && (
                  <p className="error"> {errors.address.message}</p>
                )}
              </Col>
              <Col>
                <TextField
                  className="col-md-12"
                  id="zipcode"
                  label="zipcode"
                  name="zipcode"
                  variant="outlined"
                  type="number"
                  onChange={(e) => setZipCode(e.target.value)}
                  value={zipCode}
                  {...register('zipcode', {
                    required: 'zipcode is required',
                  })}
                />
                {errors.zipCode && (
                  <p className="error"> {errors.zipCode.message}</p>
                )}{' '}
              </Col>
            </Row>
            <Row className="mb-2   bill">
              <Col className="mx-auto">
                <TextField
                  className="col-md-12"
                  id="city"
                  label="city"
                  name="city"
                  variant="outlined"
                  onChange={(e) => setCity(e.target.value)}
                  value={city}
                  {...register('city', {
                    required: 'city is required',
                  })}
                />
                {errors.city && <p className="error"> {errors.city.message}</p>}
              </Col>
              <Col>
                <TextField
                  className="col-md-12"
                  id="country"
                  label="country"
                  name="country"
                  variant="outlined"
                  onChange={(e) => setCountry(e.target.value)}
                  value={country}
                  {...register('country', {
                    required: 'country is required',
                  })}
                />
                {errors.country && (
                  <p className="error"> {errors.country.message}</p>
                )}{' '}
              </Col>
            </Row>

            <Button
              className="btn_next col-6 float-left mt-3"
              color="primary"
              variant="contained"
              type="submit"
            >
              Continue
              <ArrowForwardSharp />
            </Button>
          </form>
        </Row>
      </div>
    </div>
  );
};

export default BillingAddress;
