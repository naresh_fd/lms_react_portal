import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Button, TextField } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';

const UserDetails = () => {
  const [UserName, setUserName] = useState();
  const [email, setemail] = useState();
  const [password, setpassword] = useState();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
  });

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard mx-auto"
        style={{
          width: '25%',
        }}
      >
        <h2 className="text-center">
          Enter your name and email address to begin
        </h2>
        <div className="card pt-5 pb-5 pl-5 pr-5">
          <form
            onSubmit={handleSubmit((data) => {
              sessionStorage.setItem('UserDetails', JSON.stringify(data));
              console.log(data);
              history.push('/companydetils');
            })}
          >
            <div className="mb-3">
              <TextField
                fullWidth
                id="UserName"
                label="UserName"
                name="UserName"
                variant="outlined"
                onChange={(e) => setUserName(e.target.value)}
                value={UserName}
                {...register('UserName', {
                  required: 'Username is required',
                  minLength: {
                    value: 5,
                    message: 'Minimum 5 Characters Required for User Name',
                  },
                })}
              />
              {errors.UserName && (
                <p className="error"> {errors.UserName.message}</p>
              )}
            </div>
            <div className="mb-3">
              <TextField
                type="email"
                fullWidth
                name="email"
                label="email"
                variant="outlined"
                value={email}
                onChange={(e) => setemail(e.target.value)}
                id="email"
                aria-invalid={errors.email ? 'true' : 'false'}
                {...register('email', {
                  required: 'Email is required',
                  pattern: {
                    value: /\S+@\S+\.\S+/,
                    message: 'Entered value does not match email format',
                  },
                })}
              />
              {errors.email && <p className="error">{errors.email.message}</p>}
            </div>
            <div className="mb-3">
              <TextField
                fullWidth
                id="Password"
                name="Password"
                label="Password"
                type="Password"
                variant="outlined"
                value={password}
                onChange={(e) => setpassword(e.target.value)}
                {...register('Password', {
                  required: 'Passwod is required',
                  minLength: {
                    value: 5,
                    message: 'Minimum 5 Characters Required for User Name',
                  },
                })}
              />
              {errors.Password && (
                <p className="error"> {errors.Password.message} </p>
              )}
            </div>
            <Button
              className="btn_next"
              color="primary"
              variant="contained"
              type="submit"
            >
              Next
            </Button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default UserDetails;
