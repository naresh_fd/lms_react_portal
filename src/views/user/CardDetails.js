import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { useForm } from 'react-hook-form';
import { Button, TextField } from '@material-ui/core';
import { ArrowForwardSharp } from '@material-ui/icons';
import { useHistory } from 'react-router-dom';
import BlankNav from '../../containers/navs/BlankNav';
import '../../assets/css/sass/views/register.scss';
import premumLogo from '../../assets/img/premumLogo.svg';

const CardDetails = () => {
  const [firstName, setfirstName] = useState();
  const [lastname, setlastName] = useState();
  const [cardnumber, setcardnumber] = useState();
  // const [zipCode, setZipCode] = useState();
  const [Expiry, setExpiry] = useState();
  const [country, setCountry] = useState();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: 'onBlur',
    reValidateMode: 'onChange',
  });

  const billingDet = JSON.parse(sessionStorage.getItem('billingDetials'));
  console.log(billingDet);

  return (
    <div className="bg">
      <BlankNav />
      <div
        className="centerCard  mx-auto"
        style={{
          width: '40%',
        }}
      >
        <Row className="card p0   ">
          <div className="card-header">
            <Row>
              <Col className="col-md-2 mr-2 pl-4">
                <img
                  src={premumLogo}
                  className="float-left pl-3"
                  alt="premium Logo"
                />
              </Col>
              <Col className="col-md-8 pl-2 text-left pl-4">
                <h2 className="align-middle m0 pt-4">CMS Pro plan</h2>
                <p className="text-muted m-0">
                  Nunc scelerisque tincidunt elit. Vestibulum non mi ipsum. Cras
                  pretium suscipit tellus sit amet aliquet. Vestibulum maximus
                  lacinia massa non porttitor.
                </p>
                <Button className="plan p1">Change Plan</Button>
              </Col>
              <Col className="price">0</Col>
            </Row>
            <hr className="p2" />
            <Row>
              <Col className="text-left address pl-5 pt-2">
                <h2 className="p0 mb-0"> Billing Address</h2>
                <p className="p0 pt-1 mb-0">
                  <span className="pr-1">{billingDet.UserName} </span>
                  <span className="pr-1">{billingDet.lastname} </span>
                </p>
                <p className="p0 mb-0">
                  <span className="pr-1">{billingDet.address} </span>
                  <span className="pr-1">{billingDet.city} </span>
                  <span className="pr-1">{billingDet.zipcode} </span>
                </p>
                <p className="pr-1 p0">{billingDet.country} </p>
              </Col>
              <Col>
                <Button variant="default">Edit</Button>
              </Col>
            </Row>
          </div>
          <form
            className="p-5"
            onSubmit={handleSubmit((data) => {
              sessionStorage.setItem('billingDetials', JSON.stringify(data));
              console.log(data);
              history.push('/companydetils');
            })}
          >
            <Row className="mb-4   bill">
              <Col className="mx-auto">
                <TextField
                  className="col-md-12"
                  id="firstname"
                  label="First Name"
                  name="firstname"
                  variant="outlined"
                  onChange={(e) => setfirstName(e.target.value)}
                  value={firstName}
                  {...register('firstname', {
                    required: 'firstname is required',
                    minLength: {
                      value: 5,
                      message: 'Minimum 5 Characters Required for User Name',
                    },
                  })}
                />
                {errors.firstname && (
                  <p className="error"> {errors.firstname.message}</p>
                )}
              </Col>
              <Col>
                <TextField
                  className="col-md-12"
                  id="lastname"
                  label="Last Name"
                  name="lastname"
                  variant="outlined"
                  onChange={(e) => setlastName(e.target.value)}
                  value={lastname}
                  {...register('lastname', {
                    required: 'lastname is required',
                    minLength: {
                      value: 5,
                      message: 'Minimum 5 Characters Required for last Name',
                    },
                  })}
                />
                {errors.lastname && (
                  <p className="error"> {errors.lastname.message}</p>
                )}{' '}
              </Col>
            </Row>
            <Row className="mb-4   bill">
              <Col className="mx-auto">
                <TextField
                  type="number"
                  className="col-md-12"
                  id="cardnumber"
                  label="cardnumber"
                  name="cardnumber"
                  variant="outlined"
                  onChange={(e) => setcardnumber(e.target.value)}
                  value={cardnumber}
                  {...register('cardnumber', {
                    required: 'cardnumber is required',
                    minLength: {
                      value: 5,
                      message: 'cardnumber should be at least 5 characters',
                    },
                  })}
                />
                {errors.cardnumber && (
                  <p className="error"> {errors.cardnumber.message}</p>
                )}
              </Col>
            </Row>
            <Row className="mb-2   bill">
              <Col className="mx-auto">
                <TextField
                  className="col-md-12"
                  id="Expiry"
                  label="Expiry Date (MM/YY)"
                  name="Expiry"
                  variant="outlined"
                  onChange={(e) => setExpiry(e.target.value)}
                  value={Expiry}
                  {...register('Expiry', {
                    required: 'Expiry is required',
                  })}
                />
                {errors.Expiry && (
                  <p className="error"> {errors.Expiry.message}</p>
                )}
              </Col>
              <Col>
                <TextField
                  className="col-md-12"
                  id="country"
                  label="country"
                  name="country"
                  variant="outlined"
                  onChange={(e) => setCountry(e.target.value)}
                  value={country}
                  {...register('country', {
                    required: 'country is required',
                  })}
                />
                {errors.country && (
                  <p className="error"> {errors.country.message}</p>
                )}{' '}
              </Col>
            </Row>

            <Button
              className="btn_next col-6 float-left mt-3"
              color="primary"
              variant="contained"
              type="submit"
            >
              Continue
              <ArrowForwardSharp />
            </Button>
          </form>
        </Row>
      </div>
    </div>
  );
};

export default CardDetails;
