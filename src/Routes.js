/* eslint-disable no-shadow */
import React, { Suspense } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import { adminRoot } from './constants/defaultValues';
import BlankPage from './views/app/blank-page';
import AccountSetup1 from './views/user/AccountSetup_1';
import AddAssociates from './views/user/AddAssociate';
import CompanyDet from './views/user/CompanyDet';
import UserDetails from './views/user/UserDetails';
import BillingAddress from './views/user/BillingAddress';
import CardDetails from './views/user/CardDetails';

export const ViewHome = React.lazy(() =>
  import(/* webpackChunkName: "views" */ './views/user/Register')
);
export const ViewApp = React.lazy(() =>
  import(/* webpackChunkName: "views-app" */ './views/app')
);

export const ViewError = React.lazy(() =>
  import(/* webpackChunkName: "views-error" */ './views/error')
);

const Routes = () => (
  <Suspense fallback={<div className="loading" />}>
    <Router>
      <Switch>
        <Route path={adminRoot} render={(props) => <ViewApp {...props} />} />
        <Route path="/blank-page" component={BlankPage} />
        <Route path="/usersignup" component={UserDetails} />
        <Route path="/companydetils" component={CompanyDet} />
        <Route path="/accountSetup" component={AccountSetup1} />
        <Route path="/addassociates" component={AddAssociates} />
        <Route path="/billingdetails" component={BillingAddress} />
        <Route path="/cardgdetails" component={CardDetails} />

        <Route
          path="/error"
          exact
          render={(props) => <ViewError {...props} />}
        />
        <Route path="/" exact render={(props) => <ViewHome {...props} />} />
        <Redirect to="/error" />
        {/*
                  <Redirect exact from="/" to={adminRoot} />
                  */}
        <Redirect to="/error" />
      </Switch>
    </Router>
  </Suspense>
);

export default Routes;
